# Svigruppo
## Note
Fate attenzione ad alcune cose:  
1. Sono appunti, per cui non garantisco la loro totale correttezza. Quindi, usate sempre la testa.
2. File esterni non sono versionati, ma il loro download è automatizzato con `make`. Per cui, consiglio di effettuare la prima build con `make` e successivamente aprire il vostro editor LaTeX.

## Download 
[Download diretto del pdf dell'ultima release](https://gitlab.com/infomasternotes/Svigruppo/-/jobs/artifacts/master/raw/Svigruppo.pdf?job=pdf)  
[Download diretto del pdf dell'ultima beta](https://gitlab.com/infomasternotes/Svigruppo/-/jobs/artifacts/develop/raw/Svigruppo.pdf?job=pdf)
