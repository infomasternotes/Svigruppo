TEXC=pdflatex
TEXF= 
TARGET=Svigruppo
RM=rm -rf
OUTEXT=pdf
DELEXT = aux log out synctex.gz* toc
DLOAD=wget 
DEPFILES = img/git-pretty.pdf img

$(TARGET).pdf: $(TARGET).tex $(DEPFILES) 
	make build
	make clean
img:
	mkdir img
img/git-pretty.pdf: img
	$(DLOAD) http://justinhileman.info/article/git-pretty/git-pretty.pdf -O img/git-pretty.pdf

.PHONY: compile build clean reset
compile:
	$(TEXC) $(TEXF) $(TARGET).tex
build:
	for i in 1 2 3 ; do \
		make compile ; \
	done
clean:
	for ext in $(DELEXT) ; do \
		$(RM) $(TARGET).$$ext ; \
	done
reset:
	$(RM) $(TARGET).$(OUTEXT)
	for file in $(DEPFILES) ; do \
		$(RM) $$file ; \
	done
	make clean
